define(
["dojo/_base/declare",
 "dijit/_Widget",
 "dijit/_TemplatedMixin"],
function(declare, _Widget, _TemplatedMixin){
    return declare(
        "zc.dojo.NativeButton",
        [_Widget, _TemplatedMixin], {
            templateString:
            "<button" +
            " data-dojo-attach-point='containerNode'" +
            " data-dojo-attach-event='onclick: onClick'" +
            "></button>",

            label: "",
            _setLabelAttr: {
                node: "containerNode",
                type: "innerHTML"
            },

            onClick: function(e) {}
        });
});
