import bobo
import boboserver
import os
import zc.htmlchecker
import zc.wsgidriver

@bobo.query("/")
def index():
    return zc.wsgidriver.html(
        scripts = [
            """
              var dojoConfig = {
                  packages: [
                     {name: "zc.dojo.NativeButton", location: "/resources/js"}
                  ],
              };
            """,
            "//ajax.googleapis.com/ajax/libs/dojo/1.9.1/dojo/dojo.js"
            ],
        )

static = boboserver.static(
    '/resources', os.path.join(os.path.dirname(__file__), 'resources'))

app = bobo.Application(bobo_resources = __name__)

def start_server(*a, **kw):
    return zc.bobodriver.start_bobo_server(app, *a, **kw)

def test_suite():
    import logging; logging.basicConfig()
    return zc.wsgidriver.TestSuite(
        'README.rst',
        app=app, checker=zc.htmlchecker.HTMLChecker())
