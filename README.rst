A plain native button widget for dojo
*************************************

The goal here is to provide a plain HTML button that can be used as a
dojo widget.

Basic usage::

  nclicked = 0; // intentionally global
  require(
      ["zc.dojo.NativeButton", "dojo/_base/window"],
      function (NativeButton, win) {
          new NativeButton(
              {
                  label: "Hello world!",
                  "class": "myclass",
                  onClick: function (e) {
                      nclicked += 1;
                  }
              }).placeAt(win.body());
  });

.. -> src

    >>> browser.get(server)

    >>> browser.execute_script(src)

    >>> wait(lambda : browser.find_elements_by_tag_name('button'))

    js> nclicked
    0

    >>> print browser.page_source
    <body>
      <button class="myclass">Hello world!</button>
    </body>

    >>> button = browser.find_element_by_tag_name('button')
    >>> button.click()

    js> nclicked
    1

    >>> button.text
    u'Hello world!'

You'll typically want to specify at least a ``label`` and an ``onClick``
handler.  You can specify other attributes, such as ``"class"`` and
``id``.

.. test id

  ::

    require(
        ["zc.dojo.NativeButton", "dojo/_base/window"],
        function (NativeButton, win) {
            new NativeButton({label: "Hi!", id: "myid",
                }).placeAt(win.body());
    });

  .. -> src

    >>> browser.execute_script(src)
    >>> button = browser.find_element_by_id('myid')
    >>> button.text
    u'Hi!'

You can specify en existing node to use using an id.  For example, if
the page contained::

    <span id="wasspan">I was there</span>

.. ->html

    >>> browser.execute_script("""
    ... dojo.place(%r, dojo.body());
    ... """ % html)

Now, if we create a button using this node, we don't have to place it,
and it will take over the original node, but retain it's orginal id
and content::

  require(
      ["zc.dojo.NativeButton"],
      function (NativeButton) {
          new NativeButton({
                  "class": "myclass",
                  onClick: function (e) {
                      nclicked -= 1;
                  }
              }, "wasspan");
  });

.. -> src

    >>> browser.execute_script(src)

And the node wil lend up looking like::

    <button id="wasspan" class="myclass">I was there</button>

.. -> expected

    >>> import zc.htmlchecker
    >>> checker = zc.htmlchecker.HTMLChecker()
    >>> checker.check(expected, browser.page_source)

    >>> button = browser.find_element_by_id('wasspan')
    >>> button.click()

    js> nclicked
    0

The button class gets defined as a global,
``zc.dojo.NativeButton``, so it can be instantiated declaratively.

.. test it

    js> !! zc.dojo.NativeButton
    True

.. changelog

  Changes
  *******

  0.1.1 (2013-10-03)
  ==================

  - Make native button subclass dijit/_Widget to fix issues with click
    events not being handled early enough.

  0.1.0 (????-??-??)
  ==================

  Initial release
